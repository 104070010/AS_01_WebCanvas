# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

My functions:
1. shapes: 有長方形、圓形、三角形三種畫筆的形狀，並可調整顏色(extra)
2. brush: 畫筆可以調整大小、顏色
3. erase: 橡皮擦可調整大小(extra)
4. text: 字可調整大小、字型、顏色(如果沒有特別調整，就跟畫筆顏色一樣)(extra)、內容
5. download: 可下載
6. reset: 會清除畫面
7. upload: 可上傳
8. undo: 回覆上一個步驟
9. redo: 回復下一個步驟
10. cursor icon: 要使用特定功能時，點擊該功能的白白，他會嚇到，代表正在使用該功能

Extra functions:
11. copy: 把目前的canvas複製並存起來
12. paste: 把最近一次複製的canvas貼上
* **這兩個功能可以達到"暫存"的效果**

